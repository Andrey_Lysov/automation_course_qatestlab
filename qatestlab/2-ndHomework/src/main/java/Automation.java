import com.sun.javafx.image.BytePixelSetter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by Andrey.Lysov on 12/5/2016.
 */
public class Automation {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver",
                "C:\\Users\\Andrey.Lysov\\Downloads\\geckodriver-v0.11.1-win64\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.bing.com/");
        WebElement searchField = driver.findElement(By.className("b_searchbox"));
        searchField.sendKeys("automation");
        WebElement searchButton = driver.findElement(By.className("b_searchboxSubmit"));
        searchButton.submit();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.numberOfElementsToBe(By.id("b_results"), 1));

        System.out.println("Page title is: " + driver.getTitle());
        List<WebElement> resultList = driver.findElements(By.className("b_algo"));
        System.out.println("Result list: ");
        for (int counter = 0; counter < resultList.size(); counter++){
            System.out.println(counter + 1 + ". " + resultList.get(counter).findElement(By.tagName("h2")).getText());
        }
        driver.quit();
    }
}